// alert("Hi Salman!");

let users = ["Captain America", "Black Panther", "Hulk", "Thor"];
console.log("Before adding hero:");
console.log(users);

function addTheUser(user) {
    users.push(user);
}

addTheUser("Hawk Eye");
console.log("After adding hero:");
console.log(users);


function getUser(index) {
    return users[index];
}

let heroFound = getUser(2);
console.log("Hero found is " + heroFound);


function deleteLastUser() {
    let lastUser = users[users.length - 1];
    users.pop();
    return lastUser;
}

const lastUser = deleteLastUser();
console.log("last user prior to deleting is " + lastUser);



function updateSuperHero(index, hero) {
    users[index] = hero;
}

updateSuperHero(2, "Lastikman");
console.log(users);



function deleteUsers() {
    users = [];
}

deleteUsers();
console.log(users);



function checkEmptyArray() {
    if (users.length > 0) {
        return false;
    } else {
        return true;
    }
}

let isUsersEmpty = checkEmptyArray();
console.log(isUsersEmpty);
